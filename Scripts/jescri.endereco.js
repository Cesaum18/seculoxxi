var getAddress = function (cep) {
    showIndicator();

    $.ajax({
        url: 'https://ceps.omegainc.com.br/getAddresbyCep.php?cep=' + cep,
        type: 'GET',
        crossDomain: true,
        success: function (response) {
            response = JSON.parse(response);

            $(".estado").val(response.estado);
            $(".cidade").val(response.cidade);
            $(".bairro").val(response.bairro);
            $(".logradouro").val(response.logradouro);
            $(".latitude").val(response.latitude);
            $(".longitude").val(response.longitude);

            hideIndicator();
        },
        error: function (request, exception) {
            console.log(request, exception);
            hideIndicator();
        }
    });
}
